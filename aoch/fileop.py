#!/usr/bin/python3

def readfile(fname):	
     openfile = open(fname, "r")
     fixfile = openfile.read().replace('\n', '')
     return fixfile

def readcharsplit(fname):
     openfile = open(fname, "r")
     fixfile = openfile.read().replace('\n', '').replace(' ', '')
     listfile = list(fixfile)
     return listfile